# Troubleshooting Lab

You have in this lab the following files, and make sure the working directory is similar to the following

```bash 
.
├── App
│   ├── svc.yaml
│   └── web.yaml
├── img
│   ├── erropage.png
│   └── works.png
├── lab.md
└── lab-start.sh

```
We will be login in to our OpenShift Cluster and create a new project for this lab; 
```bash
$ oc login -u USERNAME -p PASSWORD  https://OCP.API.URL
$ oc new-project troubleshooting-lab
```

Start the lab by running the ``lab-start.sh`` script using the command below: 

```bash 
bash lab-start.sh
```
The script will present this output

```bash
 Now we have an application running on the following url, yet we can not access the application! 
        Do your best to fix the issue and get the application working!
 
 Open in your browser:      "URL based on your Domain"  
```
Open the url in your browser we should expect this error below 

![PAGE](img/erropage.png "Error") 

Let's check the running resources and start troubleshooting, using the following command we list all resources. 
```bash 
# run this command 
$ oc get all -l app=web

##this is an example 

NAME                     READY     STATUS             RESTARTS   AGE
pod/web-9b4d7d8c-f6qqb   0/1       ImagePullBackOff   0          11m

NAME          TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
service/web   ClusterIP   10.217.4.91   <none>        5050/TCP   11m

NAME                  READY     UP-TO-DATE   AVAILABLE   AGE
deployment.apps/web   0/1       1            0           11m

NAME                           DESIRED   CURRENT   READY     AGE
replicaset.apps/web-9b4d7d8c   1         1         0         11m

NAME                           HOST/PORT                   PATH      SERVICES   PORT        TERMINATION   WILDCARD
route.route.openshift.io/web   web-lab1.apps-crc.testing             web        5050-8080                 None
```
By looking at the pod we see there is an error ``ImagePullBackOff``, we can run the following commands to investigate the issue. 
```bash
$ oc describe pods web-9b4d7d8c-f6qqb # use your pod name
# output
...output omitted...

Events:
  Type     Reason          Age                 From                         Message
  ----     ------          ----                ----                         -------
  Normal   Scheduled       24m                 default-scheduler            Successfully assigned lab1/web-9b4d7d8c-f6qqb to crc-ctj2r-master-0
  Normal   AddedInterface  24m                 multus                       Add eth0 [10.217.0.125/23]
  Normal   Pulling         22m (x4 over 24m)   kubelet, crc-ctj2r-master-0  Pulling image "quay.oi/centos7/httpd-24-centos7"
  Warning  Failed          22m (x4 over 24m)   kubelet, crc-ctj2r-master-0  Failed to pull image "quay.oi/centos7/httpd-24-centos7": rpc error: code = Unknown desc = error pinging docker registry quay.oi: Get "https://quay.oi/v2/": dial tcp: lookup quay.oi on 10.88.0.8:53: no such host
  Warning  Failed          22m (x4 over 24m)   kubelet, crc-ctj2r-master-0  Error: ErrImagePull
  Normal   BackOff         14m (x44 over 24m)  kubelet, crc-ctj2r-master-0  Back-off pulling image "quay.oi/centos7/httpd-24-centos7"
  Warning  Failed          4m (x90 over 24m)   kubelet, crc-ctj2r-master-0  Error: ImagePullBackOff
```
From the event section we can see the image can't be pulled down due to error in the image registry domain name ``"https://quay.oi/v2/": dial tcp: lookup quay.oi on 10.88.0.8:53: no such host``

The correct domain is ``https://quay.io/``, now we will fix it in the deployment as follow; 

```bash 
$ oc edit deployment web
...output omitted...
    spec:
      containers:
      - image: quay.oi/centos7/httpd-24-centos7 # change the quay.oi > quay.io
        imagePullPolicy: Always
        name: httpd-24
...output omitted...
```
Save and exit, then run ``oc get pods`` to check if the new pod created. 
Once pod is running we can check if the url in our browser is accessible or not!

***STILL NOT ACCESSIBLE!***

Let's check the service running on our lab by running the following command; 
```bash
$ oc describe svc web
# output
Name:              web
Labels:            app=web
Annotations:       <none>
Selector:          app=web1
Type:              ClusterIP
IP:                10.217.4.91
Port:              5050-8080  5050/TCP
TargetPort:        8080/TCP
Endpoints:         <none> 
Session Affinity:  None
Events:            <none>
```
As you can see the ``Endpoints`` is showing ``<none>``, which mean the services is not mapping correctly to the pod. 

> NOTE: the Selector is showing app=web1 
We can check the pods labels by running 
```bash
$ oc describe pods web-xxxxxxxx | grep -i label
#output
Labels:             app=web
```
Fix the selector to match the pod label by using the following command; 
```bash 
$ oc edit svc web
...output omitted...
spec:
  clusterIP: 10.217.4.91
  ports:
  - name: 5050-8080
    port: 5050
    protocol: TCP
    targetPort: 8080
  selector:
    app: web # change it from web1 > web
  sessionAffinity: None
  type: ClusterIP
...output omitted...
```
Save and Exit. 

Now let's see if we got the correct mapping endpoint and rerun the below command; 
```bash
$ oc describe svc web
# output
Name:              web
Labels:            app=web
Annotations:       <none>
Selector:          app=web1
Type:              ClusterIP
IP:                10.217.4.91
Port:              5050-8080  5050/TCP
TargetPort:        8080/TCP
Endpoints:         10.217.0.137:8080  # you should see pod IP here
Session Affinity:  None
Events:            <none>
```
Once you see the endpoint let's go back to our browser to check our application. 
Refresh the page! Yaaay! Great now it's WORKING !! 

![PAGE](img/works.png "WORKING")

We are done with this lab you can clean up by deleting the project.
```bash
$ oc delete all -l app=web
$ oc delete project troubleshooting-lab
```


