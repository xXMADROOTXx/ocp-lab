#!/bin/bash

# #  ___ ____  __  __    ____ ____  __  __       _____ __  __    _    ____  
# # |_ _| __ )|  \/  |  / ___/ ___||  \/  |  _  | ____|  \/  |  / \  |  _ \ 
# #  | ||  _ \| |\/| | | |   \___ \| |\/| | (_) |  _| | |\/| | / _ \ | | | |
# #  | || |_) | |  | | | |___ ___) | |  | |  _  | |___| |  | |/ ___ \| |_| |
# # |___|____/|_|  |_|  \____|____/|_|  |_| (_) |_____|_|  |_/_/   \_\____/ 
# #                                                                         
# #     _    _     __  __ _____ ____ _____  _    ____ ___ 
# #    / \  | |   |  \/  | ____/ ___|_   _|/ \  |  _ \_ _|
# #   / _ \ | |   | |\/| |  _| \___ \ | | / _ \ | | | | | 
# #  / ___ \| |___| |  | | |___ ___) || |/ ___ \| |_| | | 
# # /_/   \_\_____|_|  |_|_____|____/ |_/_/   \_\____/___|


#  _____________________________
# < OCP Troubleshooting Lab! >
#  -----------------------------
#    \
#     \
#         .--.
#        |o_o |
#        |:_/ |
#       //   \ \
#      (|     | )
#     /'\_   _/`\
#     \___)=(___/
# 


# create deployment from file 
oc create -f App/web.yaml > /dev/null 2>&1
#
sleep 5 
# create service from file 
oc create -f App/svc.yaml > /dev/null 2>&1

# create route from file 
oc expose svc web > /dev/null 2>&1


### Explain ### 

##MSG##

echo " Now we have an application running on the following url, yet we can not access the application! 
        Do your best to fix the issue and get the application working!"
echo " "
# show route in terminal
URL=$(oc get route | grep web | awk -F " " '{print $2}')
echo " Open in your browser:      $URL  "
echo " "
